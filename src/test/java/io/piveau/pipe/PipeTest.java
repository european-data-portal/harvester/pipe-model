package io.piveau.pipe;

import io.piveau.pipe.model.Pipe;
import io.piveau.pipe.utils.PipeFactory;
import org.testng.annotations.Test;

public class PipeTest {

    @Test
    public void createPipe() {
        Pipe pipe = PipeFactory.create();
        assert pipe != null;
    }

}

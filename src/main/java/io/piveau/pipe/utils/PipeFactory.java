package io.piveau.pipe.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.piveau.pipe.model.Pipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class PipeFactory {

    private static final Logger log = LoggerFactory.getLogger(PipeFactory.class);

    public static Pipe create() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("pipe.json")) {
            Pipe pipe = new ObjectMapper().readValue(inputStream, Pipe.class);
            pipe.getHeader().setId(UUID.randomUUID().toString());
        } catch (IOException e) {
            log.error("reading minimum pipe from resource", e);
        }
        return null;
    }

}

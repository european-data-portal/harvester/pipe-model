package io.piveau.pipe.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.model.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Comparator;
import java.util.Optional;

public class PipeManager {

    private Pipe pipe;

    private Segment currentSegment;

    public static PipeManager create(Pipe pipe) {
        return new PipeManager(pipe);
    }

    public static PipeManager create(String resource) throws IOException {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource)) {
            Pipe pipe = new ObjectMapper().readValue(inputStream, Pipe.class);
            return new PipeManager(pipe);
        }
    }

    private PipeManager(Pipe pipe) {
        this.pipe = pipe;
        Optional<Segment> seg = pipe.getBody().getSegments().stream().filter(s -> !s.getHeader().getProcessed()).min(Comparator.comparing(s -> s.getHeader().getSegmentNumber()));
        if (seg.isPresent()) {
            currentSegment = seg.get();
        }
    }

    public Pipe getPipe() {
        return pipe;
    }

    public String getTitle() {
        return currentSegment.getHeader().getTitle();
    }

    public JsonNode getConfig() {
        return currentSegment.getBody().getConfig();
    }

    public Segment getCurrentSegment() {
        return currentSegment;
    }

    public void finished(boolean processed) {
        currentSegment.getHeader().setProcessed(processed);
    }

    public String getStringData() {
        return currentSegment.getBody().getPayload().getBody().getData();
    }

    public boolean isBase64Payload() {
        PayloadHeader header = currentSegment.getBody().getPayload().getHeader();
        return header.getDataType() == DataType.base64;
    }

    public byte[] getBinaryData() {
        String data = currentSegment.getBody().getPayload().getBody().getData();
        PayloadHeader header = currentSegment.getBody().getPayload().getHeader();
        if (header.getDataType() == DataType.base64) {
            return Base64.getDecoder().decode(data);
        } else {
            return null;
        }
    }

    public String getDataMimeType() {
        return currentSegment.getBody().getPayload().getBody().getDataMimeType();
    }

    public ObjectNode getDataInfo() {
        return currentSegment.getBody().getPayload().getBody().getDataInfo();
    }

    public void setPayloadData(byte[] data) {
        setPayloadData(Base64.getEncoder().encodeToString(data), DataType.base64, null, null);
    }

    public void setPayloadData(String data) {
        setPayloadData(data, DataType.text, null, null);
    }

    public void setPayloadData(byte[] data, ObjectNode dataInfo) {
        setPayloadData(Base64.getEncoder().encodeToString(data), DataType.base64, null, dataInfo);
    }

    public void setPayloadData(String data, ObjectNode dataInfo) {
        setPayloadData(data, DataType.text, null, dataInfo);
    }

    public void setPayloadData(byte[] data, String dataMimeType, ObjectNode dataInfo) {
        setPayloadData(Base64.getEncoder().encodeToString(data), DataType.base64, dataMimeType, dataInfo);
    }

    public void setPayloadData(String data, String dataMimeType, ObjectNode dataInfo) {
        setPayloadData(data, DataType.text, dataMimeType, dataInfo);
    }

    private void setPayloadData(String data, DataType dataType, String dataMimeType, ObjectNode dataInfo) {
        Optional<Segment> next = getNextSegment();
        if (next.isPresent()) {
            SegmentBody body = next.get().getBody();
            Payload payload = new Payload();
            payload.getHeader().setDataType(dataType);
            payload.getBody().setDataMimeType(dataMimeType);
            payload.getBody().setData(data);
            payload.getBody().setDataInfo(dataInfo);
            body.setPayload(payload);
        }
    }

    private Optional<Segment> getNextSegment() {
        return pipe.getBody().getSegments().stream().filter(s -> !s.getHeader().getProcessed() && s.getHeader().getSegmentNumber() > currentSegment.getHeader().getSegmentNumber()).min(Comparator.comparing(s -> s.getHeader().getSegmentNumber()));
    }

    public Endpoint getNextEndpoint() {
        Optional<Segment> segment = getNextSegment();
        return segment.isPresent() ? segment.get().getBody().getEndpoint() : null;
    }

    public Pipe processedPipe() {
        finished(true);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Pipe deepCopy = objectMapper.readValue(objectMapper.writeValueAsString(pipe), Pipe.class);
            return deepCopy;
        } catch (IOException e) {
            return null;
        } finally {
            finished(false);
        }
    }

}

package io.piveau.pipe.validation;

public class PipeValidationException extends Throwable {

    PipeValidationException(Throwable cause) {
        super(cause);
    }

}

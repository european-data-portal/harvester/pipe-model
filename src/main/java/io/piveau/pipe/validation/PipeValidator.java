package io.piveau.pipe.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.piveau.pipe.model.Pipe;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

public class PipeValidator {

    private static Logger log = LoggerFactory.getLogger(PipeValidator.class);

    private static Schema schema;

    static {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("piveau-pipe.schema.json")) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            schema = SchemaLoader.load(rawSchema);
        } catch (IOException e) {
            log.error("reading schema", e);
        }
    }

    public static void validate(Pipe pipe) throws PipeValidationException {
        try {
            schema.validate(new JSONObject(new ObjectMapper().writeValueAsString(pipe)));
        } catch (JsonProcessingException e) {
            log.error("json to string", e);
            throw new PipeValidationException(e);
        } catch (ValidationException e) {
            log.error("validating json", e);
            throw new PipeValidationException(e);
        }
    }

}

package io.piveau.pipe.model;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Pipe {

    private PipeHeader header = new PipeHeader();

    private PipeBody body = new PipeBody();

    private Map<String, JsonNode> additionalProperties = new HashMap<>();

    public PipeHeader getHeader() {
        return header;
    }

    public void setHeader(PipeHeader header) {
        this.header = header;
    }

    public PipeBody getBody() {
        return body;
    }

    public void setBody(PipeBody body) {
        this.body = body;
    }

    @JsonAnyGetter
    public Map<String, JsonNode> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String property, JsonNode value) {
        additionalProperties.put(property, value);
    }

}

package io.piveau.pipe.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Segment {

    private SegmentHeader header = new SegmentHeader();

    private SegmentBody body = new SegmentBody();

    public SegmentHeader getHeader() {
        return header;
    }

    public void setHeader(SegmentHeader header) {
        this.header = header;
    }

    public SegmentBody getBody() {
        return body;
    }

    public void setBody(SegmentBody body) {
        this.body = body;
    }

}

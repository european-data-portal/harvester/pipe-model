package io.piveau.pipe.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PayloadBody {

    private String key;

    private String cypher;

    private String dataMimeType;

    private String data;

    private ObjectNode dataInfo;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCypher() {
        return cypher;
    }

    public void setCypher(String cypher) {
        this.cypher = cypher;
    }

    public String getDataMimeType() {
        return dataMimeType;
    }

    public void setDataMimeType(String dataMimeType) {
        this.dataMimeType = dataMimeType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ObjectNode getDataInfo() {
        return dataInfo;
    }

    public void setDataInfo(ObjectNode dataInfo) {
        this.dataInfo = dataInfo;
    }

}

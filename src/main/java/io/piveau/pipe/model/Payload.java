package io.piveau.pipe.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Payload {

    private PayloadHeader header = new PayloadHeader();

    private PayloadBody body = new PayloadBody();

    public PayloadHeader getHeader() {
        return header;
    }

    public void setHeader(PayloadHeader header) {
        this.header = header;
    }

    public PayloadBody getBody() {
        return body;
    }

    public void setBody(PayloadBody body) {
        this.body = body;
    }

}

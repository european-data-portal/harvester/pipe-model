# pipe model

## Table of Contents
1. [Build & Install](#build-install)
1. [Use](#use)
1. [Interface](#interface)
1. [License](#license)

## Build & Install
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/pipe-model.git
$ cd pipe-model
$ mvn install
```

## Use
Add dependency to your project pom file:
```xml
<dependency>
    <groupId>io.piveau</groupId>
    <artifactId>pipe-model</artifactId>
    <version>4.3.1</version>
</dependency>
```

## Interface

The documentation of the REST interface can be found when the root context is opened in a browser.

## License

[Apache License, Version 2.0](LICENSE.md)
